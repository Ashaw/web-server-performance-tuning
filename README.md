# Performance Tuning

Most of the information in this is based on [these guides](https://www.liquidweb.com/kb/series/apache-performance-tuning/).

## Swap Memory

Swap files are similar to page files on Windows and are used as an extension of standard memory space (RAM) onto much slower physical drives (HDD, SDD, NVMe). An over-tuned server may eventually lead to an overuse of swap files which will likely result in a performance bottleneck.

This type of bottleneck is known as a thrashing point or *ServerLimit*. It is important to determine this value because it provide a limiting value for further configurations and prevent a bottleneck based purely on the resources allocated to the server.

Furthermore, if the configuration of the rest of the system is determined to be optimal yet the performance is still degraded, then additional resources should be allocated to the machine to raise the *ServerLimit*. After that, the configurations can be modified to match this new resource limit.

Estimating the *ServerLimit* should be done during peak operations hours and periodically reassessed during the performance tuning process. It is recommended that this type of load testing and performance tuning is done using non-production load testing that accurately simulates typical system usage.

The calculation for the *ServerLimit* is as follows:
>( buff/cache - Reserved ) % Avg.Apache

1. **buff/cache**: The total memory used by the Kernel for buffers and cache.\
`$ free`
2. **Reserved**: The amount of memory reserved for non-Apache processes. This may be anywhere between 0%-25% of the buff/cache memory value depending on what else the server does.
3. **Available**: The difference between buff/cache and Reserved memory.
4. **Avg.Apache**: The average of all running Apache children during peak operational hours.\
`$ ps o rss= -C apache2 | awk '{n+=$1}END{print n/NR}'`

The calculated *ServerLimit* (Thrashing Point) value is the amount of Apache Children the server can handle.

Example:

![Thrashing Point](https://res.cloudinary.com/lwgatsby/f_auto/www/uploads/2018/09/Thrashingpointtable.jpg)

Conservative estimates may be used to determine a good starting point to use for how much memory your machine should be allocated. 
> 1 GB Memory per 12 Simultaneous Requests

A properly tuned system may handle up to 24req/GB or even as much as 48req/GB.

Another, less optimal, way to determine memory allocation would be to use the *memory_limit* setting (if running PHP) for each request/process. Then multiplying this by the number of estimated users during peak hours. However, the default value for *memory_limit* is 256MB which will end up being 1 GB Memory per 4 Simultaneous Requests (a very conservative estimate/not recommended).\
**Note**: Using this value can be a great way to configure a resource limited environment and rule out the possibility of an over-tuned web server whereby poor performance is the result of bad code or other issues not discussed in this document.

[Documentation](https://www.liquidweb.com/kb/apache-performance-tuning-swap-memory/)

## MPM Modules

The keystone for understanding Apache server performance is by far the Multiprocessing Modules (MPMs). These modules determine the basis for how Apache addresses multiprocessing. Multiprocessing means running multiple operations simultaneously in a system with multiple central processing units (CPU Cores). 

[Documentation](https://www.liquidweb.com/kb/apache-performance-tuning-apache-mpm-modules/)

### General Optimization

These are global settings located in the Apache configuration file.\
`$ sudo nano /etc/apache2/apache2.conf`

- **Timeout**: The numerical value of seconds Apache waits for all common I/O events. Apache will abandon requests fail to complete before the provided Timeout value.

Determining the right Timeout depends on both traffic habits and hosted applications. Ideally, Timeout should be as low as possible while still allowing the vast majority of regular traffic to operate without issue. Large timeouts, those above 1 minute, open the server to SlowLoris style DOS attacks and foster a long wait in the browser when it encounters a problem. Lower timeouts allow Apache to recover from errant stuck connections quickly. It becomes necessary to strike a balance between the two extremes.

- **KeepAlive**: A simple on|off toggle enables the *KeepAlive* protocols with supported browsers. The *KeepAlive* feature can provide as much as a 50% reductions in latency, significantly boosting the performance of Apache. *KeepAlive* accomplishes this by reusing the same initial connections a browser creates when connecting to Apache for all follow-up requests which occur within a short period.

*KeepAlive* is a powerful feature and in general, should be enabled in most situations. It works great for reducing some of the CPU and Network overhead with modern element heavy websites. For example, an easy way to visualize *KeepAlive* is with the “hold the door” phrase. Imagine a queue of people entering a building through a single doorway. Each person is required to open the door, walk through it, then close the door before the next person does the same process. Mostly, that's how Apache works without *KeepAlive*. When enabled, the door stays open until all the people in line are through the door before it closes again.

Two additional related directives also govern *KeepAlive*. MaxKeepAliveRequests and KeepAliveTimeout. Discussed in the next section, each one plays a vital role in fine-tuning of the *KeepAlive* directive.

- **MaxKeepAliveRequests**: Sets a limit on the number of requests an individual *KeepAlive* connection is permitted to handle. Once reached, Apache forces the connection to terminate, and creates a new one for any additional requests.

Determining an ideal setting here is open to interpretation. Generally, you want this value to be at least as high as the largest count of elements (HTML, Text, CSS, Images, Etc..) served by the most heavily trafficked pages on the server.

Set *MaxKeepAliveRequests* to double that of the largest count of elements on common pages.

- **KeepAliveTimeout**: This directive is measured in seconds and will remain idle waiting for additional requests from its initiator. Since these types of connections are only accessible to their initiator, we want to keep *KeepAliveTimeout* very low. A low value prevents too many *KeepAlive* connections from locking out new visitors due to connection priority.

A large *MaxKeepAliveRequests* directive with a very low *KeepAliveTimeout* allows active visitors to reuse connections while also quickly recovering threads from idle visitors. **Configuration**: Set *MaxKeepAliveRequests* to 500+, Set *KeepAliveTimeout* to 2 **Requirements**: Works best on MPM Event.

### MPM Prefork

- Self-regulated
- Non-threaded process-based approach at multiprocessing
- Single master parent process governing all children
- Each child server handles only a single request
- Used for compatibility when non-threaded libraries/software are required
- Requires vastly more resources to reach similar traffic levels as a threaded MPM

**Note**: Avoid using MPM Prefork whenever possible. It’s inability to scale well with increased traffic will quickly outpace the available hardware on most system configurations.

[More info](https://httpd.apache.org/docs/current/mod/prefork.html)

### MPM Worker

- Hybrid pre-forking
- Multi threaded
- Multiprocessing
- Single master parent process governing all children
- Children are multi-threaded processes that can handle dozens of threads (requests) simultaneously
- High capacity and low resource solution
- Shares configuration directives with MPM Event

**Note**: The KeepAliveTimeOut directive currently defines the amount of time Apache will wait for requests. When utilizing KeepAlive with MPM Worker use the smallest KeepAliveTimeout as possible (1 second preferably).

[More info](https://httpd.apache.org/docs/current/mod/worker.html)

### MPM Event

- Shares configuration directives with MPM Worker
- Works nearly identical to MPM Worker except when it comes to handling KeepAlive requests
- Uses a dedicated Listener thread in each child process
- Listening thread is responsible for directing incoming requests to an available worker thread
- Listening thread solves the issue encountered by MPM Worker which locks entire threads into waiting for the KeepAliveTimeout
- Listener approach of MPM Event ensures worker threads are not “stuck” waiting for KeepAliveTimeout to expire
- Keeps the maximum amount of worker threads handling as many requests as possible

**Note**: MPM Event is stable in Apache 2.4, older versions can use MPM Worker as an alternative.

[More info](https://httpd.apache.org/docs/current/mod/event.html)

## MPM Directives

- [ServerLimit](https://httpd.apache.org/docs/current/mod/mpm_common.html#ServerLimit): Upper limit on configurable number of processes.
- [StartServers](https://httpd.apache.org/docs/current/mod/mpm_common.html#StartServers): Number of child server processes created at startup.
- [MinSpareThreads](https://httpd.apache.org/docs/current/mod/mpm_common.html#MinSpareThreads): Minimum number of idle threads available to handle request spikes.
- [MaxSpareThreads](https://httpd.apache.org/docs/current/mod/mpm_common.html#MaxSpareThreads): Maximum number of idle threads.
- [ThreadLimit](https://httpd.apache.org/docs/current/mod/mpm_common.html#threadlimit): Sets the upper limit on the configurable number of threads per child process.
- [ThreadsPerChild](https://httpd.apache.org/docs/current/mod/mpm_common.html#threadsperchild): Number of threads created by each child process.
- [MaxRequestWorkers](https://httpd.apache.org/docs/current/mod/mpm_common.html#MaxRequestWorkers): Maximum number of connections that will be processed simultaneously.
- [MaxConnectionsPerChild](https://httpd.apache.org/docs/current/mod/mpm_common.html#MaxConnectionsPerChild): Limit on the number of connections that an individual child server will handle during its life. This only needs to be set to a value greater than 0 if memory leaks are suspected.

[Apache MPM Common Directives](https://httpd.apache.org/docs/current/mod/mpm_common.html)

[Documentation](https://www.liquidweb.com/kb/apache-performance-tuning-mpm-directives/)

## Configuring MPM Directives

The MPM module you choose will be configured similar to what is shown below. To configure these settings use:

`$ sudo nano /etc/apache2/sites-enabled/mpm-[event/worker].conf`

```
ServerLimit             16
StartServers            2
MinSpareThreads         64
MaxSpareThreads         128
ThreadLimit             64
ThreadsPerChild         64
MaxRequestWorkers       128
MaxConnectionsPerChild  0
```

[Documentation](https://www.liquidweb.com/kb/apache-performance-tuning-configuring-mpm-directives/)

## Apache Caching

Most web sites have large amounts of content that remains unchanged or is rarely modified after publication. And everytime it is requested from the web server, modified or not, it is reprocessed and transmitted to the client, unnecessarily consuming valuable system resources and network bandwidth. As your website or application gains in popularity, more demand we be put on your server to process content that is, although dynamically generated, static.

Apache comes with three modules for caching content, one enables it and the remaining two determine where the cache store exists – on disk or in memory. Determining which module to use for the cache store depends on your available hardware resources and performance requirements.

[Documentation](https://httpd.apache.org/docs/2.4/mod/mod_cache.html)

### mod_cache

Caching provides a mechanism to verify whether stale or expired content is still fresh, and can represent a significant performance boost.

To enable the Apache caching mod:\
`$ sudo a2enmod cache`

Here is a typical configuration for caching in the vhost file of a site.


```{XML}
        <IfModule mod_cache.c>
            CacheQuickHandler off
            CacheDisable /wp-admin
            <IfModule mod_cache_disk.c>
                CacheRoot "/var/cache/apache2/mod_cache_disk"
                CacheEnable disk  "/"
                CacheDirLevels 5
                CacheDirLength 3
            </IfModule>
            <IfModule mod_cache_socache.c>
                CacheSocache shmcb
                CacheSocacheMaxSize 102400
                CacheEnable socache /
            </IfModule>
           <IfModule mod_expires.c>
                ExpiresActive On
                ExpiresDefault "access plus 1 day"
           </IfModule>
        </IfModule>
        CustomLog /var/log/apache2/app-cache.log cachelog
```

### mod_cache_disk

The headers and bodies of cached responses are stored separately on disk, in a directory structure derived from the md5 hash of the cached URL.

[Documentation](https://httpd.apache.org/docs/2.4/mod/mod_cache_disk.html)

`$ sudo a2enmod cache_disk`

### mod_cache_socache

The headers and bodies of cached responses are combined, and stored underneath a single key in the shared object cache.

[Documentation](https://httpd.apache.org/docs/2.4/en/mod/mod_cache_socache.html)

`$ sudo a2enmod mod_cache_socache`

### mod_expires

This module allows you to define expiration dates for your content, as a whole or individually based on type or matching string.

```{XML}
<IfModule mod_expires.c>
        ExpiresActive On
        ExpiresDefault "access plus 1 day"
        ExpiresByType image/jpg "access plus 5 days"
        ExpiresByType image/jpeg "access plus 5 days"
        ExpiresByType image/gif "access plus 5 days"
        ExpiresByType image/png "access plus 5 days"
        ExpiresByType text/css "access plus 1 month"
        ExpiresByType application/pdf "access plus 1 month"
        ExpiresByType text/x-javascript "access plus 1 month"
        ExpiresByType application/x-shockwave-flash "access plus 1 month"
        ExpiresByType image/x-icon "access plus 1 year"
</IfModule>
```

### Cache Control Headers

Cache-Control are HTTP cache headers that holds instructions for caching for requests and responses. It is used to defines how a resource is cached, where it’s cached and its maximum age before expiring. When you visit a website, your browser will save images and website data in a store called the cache. When you revisit the same website again, cache-control sets the rules that determine whether your resources loaded from your local cache or if the browser should send a request to the server for fresh resources.

The most common cache-control headers are detailed below:

**Cache-Control: Public**

This directive indicates that the response may be stored by any cache, even if the response is normally non-cacheable.

**Cache-Control: Private**

This directive indicates that the response can only be cached by the browser that is accessing the file. It can not be cached by an intermediary agent such as proxy or CDN.

**Cache-Control: Max-Age**

This directive indicates that the maximum amount of time a resource is considered fresh. In other words how many seconds a resource can be served from cache after it's been downloaded. For example, if the max age is set to 3600 means that the returned resource is valid for 3600 seconds, after which the browser has to request a newer version.

You can also use a technique developed by some assets builders tools, like Webpack or Gulp to force the browser to download a new version of the needed file. This will precompiled each file on the server and add hash sums to the file names, such as “app-72420c47cc.css”. So, after next the deployment, you will get a new version of the file.

**Cache-Control: No-Cache**

This directive indicates that a browser may cache a response, but must first submit a validation request to an origin server. This directive is not effective in preventing caches from storing your response. It allows you to cache but subsequence response or any subsequence response for similar data the client needs to check with the browser whether that resource has changed or not. Only if the resource has not changed then the client serves the cache which is stored.

If you apply the technique you learned in the previous section in html files, you will never get new links for your css, js, or image files until you force a reload.

It is recommended to use Cache-Control: no-cache to html files to validate resources on the server before use it from the cache.

**Cache-Control: No-Store**

This directive indicates that the response should never be cached, For example, banking details you would not want to be stored in any browser cache. For those kinds of purposes, you can use no-store.

**Apache Implementation**

For the Apache web server, you will need edit your website virtual host configuration file in order to implement the HTTP Cache-Control header, for example:

```{XML}
<filesMatch ".(ico|pdf|flv|jpg|jpeg|png|gif|js|css|swf)$">
    Header set Cache-Control "max-age=3600, public"
</filesMatch>
```

[Documentation](https://webdock.io/en/docs/webdock-control-panel/optimizing-performance/setting-cache-control-headers-common-content-types-nginx-and-apache)

# Monitoring

## Logs

It is generally a best practice to have a server dedicated to system logging or a similar cloud log repository. 

Having too much logging is not usually a concern, but to determine how much disk space is being taken up by logs use the following command:\
`$ sudo du -hs /var/log`

Be sure to configure log rotation based on what is outlined in the SLA.\
>/etc/logrotate.d/

**Graylog**: A log management system that provides a means to aggregate, organize, and make sense of all log related data. [More info](https://www.graylog.org/)

**dmesg**: Used to examine or control the kernel ring buffer.\
`$ sudo dmesg`

**journalctl**: Handles all of the messages produced by the kernel, initrd, services, etc. Centralizes the management of logs. **Note**: This is not a suitable location to capture high-throughput logs like Apache access logs.\
`$ journalctl -xef`\
`$ journalctl -xe -u apache.service -o json-pretty`

**Apache access logs**: These logs will show details about each request being made to the Apache web server. It may be a good idea to disable this when load-testing as logging all requests could fill up the disk. The following line will need to be present in the vhost file:
```
CustomLog ${APACHE_LOG_DIR}/somesite-access.log combined
```
Now view the access log in real-time:\
`$ sudo tail -f /var/log/apache2/somesite-access.log`

[Documentation](https://httpd.apache.org/docs/current/logs.html#accesslog)

**Apache error logs**: These logs will show details about errors in the web server. The following line will need to be present in the vhost file:
```
ErrorLog ${APACHE_LOG_DIR}/somesite-error.log
```

Now view the error log in real-time:\
`$ sudo tail -f /var/log/apache2/somesite-error.log`

## htop

Interactive process viewer.

![htop](https://htop.dev/images/htop-2.0.png)

## Netdata

[Netdata](https://www.netdata.cloud/) is real-Time data collection and visualization.

Setup is incredibly easy and no configuration is required. Alerts show up in cloud web UI and can be configured to be sent through e-mail (free) and through various other notification methods including [Microsoft Teams](https://learn.netdata.cloud/docs/alerts-and-notifications/notifications/agent-alert-notifications/microsoft-teams) with a [paid subscription](https://blog.netdata.cloud/introducing-netdata-paid-subscriptions/).

`$ wget -O /tmp/netdata-kickstart.sh https://my-netdata.io/kickstart.sh && sh /tmp/netdata-kickstart.sh --disable-telemetry`

![Overview](https://user-images.githubusercontent.com/1153921/108732681-09791980-74eb-11eb-9ba2-98cb1b6608de.png)

![Interactive](https://user-images.githubusercontent.com/1153921/99315558-0e5d6b80-2820-11eb-91e9-9c46bc4c7298.gif)

[Documentation](https://learn.netdata.cloud/docs/getting-started/install-netdata)

## mod_status

Have a default HTTP site bound to localhost. You may need to install some implementation of *www-browser* like *Lynx*.

`$ sudo a2enmod status`\
`$ sudo apachectl status`

Output will look something like this:

```
Apache Server Status for localhost (via 127.0.0.1)

Server Version: Apache/2.4.56 (Ubuntu) mod_fcgid/2.3.9 OpenSSL/3.0.2
Server MPM: event
Server Built: 2023-03-09T07:34:31

-------------------------------------------------------------------------------

Current Time: Wednesday, 05-Apr-2023 15:32:15 EDT
Restart Time: Tuesday, 04-Apr-2023 16:31:38 EDT
Parent Server Config. Generation: 2
Parent Server MPM Generation: 1
Server uptime: 23 hours 37 seconds
Server load: 0.03 0.20 0.24
Total accesses: 127382 - Total Traffic: 915.8 MB - Total Duration: 146109300
CPU Usage: u3.49 s1.42 cu52.53 cs17.46 - .0904% CPU load
1.54 requests/sec - 11.3 kB/second - 7.4 kB/request - 1147.02 ms/request
5 requests currently being processed, 123 idle workers

Slot  PID     Stopping      Connections    Threads      Async connections
                          total accepting busy idle writing keep-alive closing
0    20206  yes (old gen) 2     no        0    0    0       0          0
1    20207  yes (old gen) 4     no        0    0    0       0          0
2    162365 no            1     yes       1    63   0       1          0
3    162296 no            4     yes       4    60   0       0          0
Sum  4      2             11              5    123  0       1          0

.............................G..................................
....G...............................G.....................G.....
_________________________________________________________W______
_____W_____________________________________W___________WW_______

Scoreboard Key:
"_" Waiting for Connection, "S" Starting up, "R" Reading Request,
"W" Sending Reply, "K" Keepalive (read), "D" DNS Lookup,
"C" Closing connection, "L" Logging, "G" Gracefully finishing,
"I" Idle cleanup of worker, "." Open slot with no current process
```

# Load Testing

>Load Testing is a type of performance testing that helps in determining how the application will behave when multiple users access it simultaneously. The goals are to break the system, find bottlenecks, and discover the limits of the system. You want to find the applications absolute limit, investigate what is going wrong, improve the code, and do it all over again.\
\
We are looking for 3 things:\
    1. **Breaking Points**: The application begins returning status code 500 and unhandled error messages.\
    2. **Throughput**: How many requests the application can get through before something breaks.\
    3. **Latency**: How long requests take.\
Credit: [Adrian Pardo](https://adrianpardo.dev/blog/using-docker-compose-and-locust-for-load-testing)

[LocastIO](https://docs.locust.io/) is a load testing tool written in Python. Locust is an easy to use, scriptable and scalable performance testing tool. You define the behaviour of your users in regular Python code, instead of being stuck in a UI or restrictive domain specific language. This makes Locust infinitely expandable and very developer friendly.

## Installation

Locust may require a significant amount of computing resources so it shouldn't be installed on the same system that will be tested. For distributed load generation it is recommended to [run Locust in Docker](https://docs.locust.io/en/stable/running-in-docker.html#running-in-docker) and run as many workers up to the amount of cores on the machine. See: [Distributed load generation](https://docs.locust.io/en/stable/running-distributed.html).

For a simple application or to observe a well known issue, it may be enough to perform a very simple installation that will only require a single core as shown below.

[Documentation](https://docs.locust.io/en/stable/installation.html#installation)

1. Install Python

>`$ sudo apt-get install python`

2. Install Locust

>`$ pip3 install locust`

3. Validate installation

>`$ locust -V`\
`locust 2.15.1 from /usr/local/lib/python3.10/site-packages/locust (python 3.10.6)`

## Create Locust Test File

A locustfile is a file written in Python that simulates how a user will use the system. These files can be handwritten, or recorded using any [software](https://en.wikipedia.org/wiki/HAR_(file_format)) that records in the HAR format and then converting that HAR file into a locustfile.py using [har2locust](https://github.com/SvenskaSpel/har2locust).

Example *locustfile.py*:

```{python}
from locust import HttpUser, task, between

class SomeTestUser(HttpUser):
    wait_time = between(1, 2)

    @task
def login_user(self):
        self.client.get("/login")
        self.client.get("/dashboard")
```

It is important to organize the tasks in the locustfiles in a way that will make the statistics generated easier to interpret.

Consider writing your tests to use **FastHttpUser** if you’re planning to run tests with very high throughput and have limited hardware. See: [Increase performance with a faster HTTP client](https://docs.locust.io/en/stable/increase-performance.html#increase-performance-with-a-faster-http-client)

[Documentation](https://docs.locust.io/en/stable/writing-a-locustfile.html)

## Statistics

Each load test run will be based on the values specified for total number of users to simulate and the spawn rate of the users per second. The spawn rate can be used to simulate how traffic might ramp-up throughout a busy time of day or to test the performance of your web server to spawn more request workers before the maximum server limit is reached. This will be discussed in more detail in the performance tuning section.

![Start](https://raw.githubusercontent.com/locustio/locust/master/locust/static/img/ui-screenshot-start-test.png)

As soon as the test begins to run, each requested will be displayed in a table grouped by the name provided for the request and the type of request (PUT, GET, POST, UPDATE). 

Columns will display each metric of interest including:

1. **# Requests**: How many times the request has been made so far in this test.
2. **# Fails**: How many times the request has resulted in a failed response status code (500). This will increase as we enter a critical state or are testing error handling.
3. **Median (ms)**: The median response time.
4. **90%ile (ms)**: How long the worst performing responses take.
5. **Average (ms)**: The average response time.
6. **Max (ms)**: The maximum response time.
7. **Average size (bytes)**: Average size of the response. Can be used to tune content caching.
8. **Current RPS**: The current requests per second. Longer response times will cause tests to run slower, but the aggregated value of this column can indicate how many total requests per second the web server is able to handle.
9. **Current Failures/s**: The current failures per second.

![Statistics](https://raw.githubusercontent.com/locustio/locust/master/locust/static/img/ui-screenshot-stats.png)

Each statistic listed above will have a corresponding chart. These give us visual point-in-time indicators of valleys, peaks, and plateaus.

![Charts](https://raw.githubusercontent.com/locustio/locust/master/locust/static/img/ui-screenshot-charts.png)
